package com.acidtango.javaboilerplate.numbers


class ComplexNumber(private val realPart: Int, private val complexPart: Int) {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as ComplexNumber

        if (realPart != other.realPart) return false
        if (complexPart != other.complexPart) return false

        return true
    }

    override fun hashCode(): Int {
        return realPart + complexPart
    }

    fun add(b: ComplexNumber): ComplexNumber {
        return ComplexNumber(realPart + b.realPart, complexPart + b.complexPart)
    }

    fun subtract(b: ComplexNumber): Any? {
        return ComplexNumber(realPart - b.realPart, complexPart - b.complexPart)
    }
}

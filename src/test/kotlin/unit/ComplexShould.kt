package unit

import com.acidtango.javaboilerplate.numbers.ComplexNumber
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test


class ComplexShould {
    @Test
    fun `be comparable to other complex`() {
        assertEquals(ComplexNumber(1, 1), ComplexNumber(1, 1))
    }

    @Test
    fun `be added to other complex number`() {
        val a = ComplexNumber(1, 1)
        val b = ComplexNumber(2, 2)

        val c = a.add(b)

        assertEquals(c, ComplexNumber(3, 3))
    }

    @Test
    fun `be subtracted to other complex number`() {
        val a = ComplexNumber(1, 1)
        val b = ComplexNumber(2, 2)

        val c = a.subtract(b)

        assertEquals(c, ComplexNumber(-1, -1))
    }
}
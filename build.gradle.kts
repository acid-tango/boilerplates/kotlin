plugins {
    kotlin("jvm") version "1.5.20"
}

group = "com.acidtango.kotlinboilerplate"
version = "1.0.0"

repositories {
    mavenCentral()
}

dependencies {
    implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8")
    testImplementation("org.junit.jupiter:junit-jupiter-api:5.8.0-M1")
    testRuntimeOnly("org.junit.jupiter:junit-jupiter-engine:5.8.0-M1")
}

tasks.test {
    useJUnitPlatform()
}

tasks.withType<Wrapper> {
    gradleVersion = "7.1.1"
}

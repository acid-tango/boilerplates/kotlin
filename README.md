# Kotlin Boilerplate

This boilerplate include:

- 💬 Kotlin 1.5
- ⚙️ Gradle 7.1
- ✅ JUnit 5.8
